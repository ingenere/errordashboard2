<?php

require_once "template/header.php";
require_once "classes/Errors.php";
?>
	<div class="jumbotron">
		<h2 class="text-center">ERRORS DASHBOARD</h2>
	</div>
	
	<div class="pull-right nav-margin-bottom">
		<?php require_once "template/nav.php"; ?>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Description</th>
							<th>Date</th>
							<th>Username</th>
							<th>Class</th>
							<th>Flag</th>
						</tr>
					</thead>
					<tbody>
						<?php  
							$errors = new Errors();
							$flag = null;
							if(!empty($_GET)){
								$flag = $_GET['flag'];
							}
							echo $errors->getErrors($flag);
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?php

require_once "template/footer.php";

?>