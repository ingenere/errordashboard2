<?php
session_start();
require_once "template/header.php";
require_once "classes/Errors.php";

?>
	<div class="jumbotron">
		<h2 class="text-center">LIST OF ERRORS</h2>
	</div>
    

	<div class="container">
	<div class="row">
		<div class="col" style="padding-left:2rem">
			<div class="row nav-margin-bottom">
				<?php require_once "template/nav.php"; ?>
			</div>
		</div>
		<div class="col pull-right">
			<?php
			$errors = new Errors();
			if(isset($_GET['flag']))
			{
				$flag = $_GET['flag'];
			}
			$page =1;
			if(isset($_GET['page'])){

				 $page = $_GET['page'];

			    }
			echo "<div class='alert alert-info text-center'><b>You are searching</b> ";
			if(isset($flag))
			switch ($flag) 
			{
				case 1:
					echo("<b>minor errors page $page</b></div>");
					break;
				case 2:
					echo("<b>major errors page $page</b></div>");
					break;
				case 3:
					echo("<b>urgent errors page $page</b></div>");
					break;
					
				}
				else{
					echo "<b>All Errors page $page</b></div>";
				}
			
			?>				
		</div>
	</div>
	
	<form method="POST" action="error_list.php" class="filter-form">
  		<div class="form-row">
  			<div class="col-sm-1">
  				<label for="exampleFormControlSelect1">Search By</label>
  			</div>
  			<div class="col-sm-2">
  				<select class="form-control" name="field">
      				<option value="PACKAGE">Package</option>
      				<option value="CLASS">Class</option>
      				<option value="METHOD">Method</option>
      				<option value="DESI">Description</option>
      				<option value="CLIENT_ID">Client ID</option>
    			</select>
             </div>
    		<div class="col-sm-2">
      			<input type="text" class="form-control" placeholder="Search Term" name="search_term">
    		</div>
    		<div class="col-sm-3">
      			<input type="date" class="form-control" placeholder="From Date" name="from_date">
    		</div>

    		<div class="col-sm-3">
      			<input type="date" class="form-control" placeholder="To Date" name="to_date">
    		</div>

    		<div class="col-sm-1">
    			<button type="submit" class="btn btn-primary"> Filter</button>
    		</div>
  		</div>
	</form>
	
	

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Date</th>
							<th>Description</th>
							<th>Package</th>
							<th>Class</th>
							<th>Method</th>
							<th>ClientID</th>
							<th>Username</th>
							<th>IP address</th>
							<th>Mac address</th>
							<th>Java version</th>
							<th>derby version</th>
							<th>Anti virus</th>
							<th>Flag</th>
							
						</tr>
					</thead>
					<tbody>
						<?php  
							
							$flag = null;
							if(isset($_GET['flag'])){
								$flag = $_GET['flag'];
							}


							echo $errors->getErrors($flag, $_POST);

						?>
					</tbody>
				</table>


      
			</div>

		</div>
		<ul class="pager">
					
                

				<?php  
							$flag =null;

							if(isset($_GET['flag'])){
								$flag = $_GET['flag'];
							}
							else if(isset($_SESSION["flag"])){
								$flag = $_SESSION["flag"];
							}
							
							$results_per_page = 20;

							$tpages =  $errors->getPages($flag);

						
							 $tpg = ceil($tpages / $results_per_page); 

            if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };

            $now=1;
            $prev=1;
            $next=1;
         

		if($page>1){
            $prev=$page-1;
            
        }
        $next=$page+1;
            echo "<a href='error_list.php?page=".$prev."' class=\"btn btn-info\">Prev</a>";



			for ($i=1; $i<=$tpg; $i++) {  // print links for all pages
			
            echo "<a href='error_list.php?page=".$i."'";
           
            if ($i==$page)  echo " class='btn btn-primary  '";
            echo ">".$i."</a> "; 
          }
         echo "<a href='error_list.php?page=".$next."' class=\"btn btn-info\">Next</a>";


						?>

		    
                
            </ul>
	</div>
</div>

<?php

require_once "template/footer.php";

?>