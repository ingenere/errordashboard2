
<?php

require_once "template/header.php";
require_once "classes/graph.php";


?>
    <div class="jumbotron">
        <h3 class="text-center">Error Graph for the month <?php ?></h3>
    </div>
    <div class="container">
       

    <form method="POST" action="error_graphs.php" class="filter-form">
        <div class="form-row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Search Term" name="search_term">
            </div>
            <div class="col">
                <input type="date" class="form-control" placeholder="From Date" name="from_date">
            </div>

            <div class="col">
                <input type="date" class="form-control" placeholder="To Date" name="to_date">
            </div>

            <div class="col">
                <button type="submit" class="btn btn-primary"> Filter</button>
            </div>
        </div>
    </form>
    

        <div class="row">
            <div class="col">
                <div id="myfirstchart"></div>
                <div id="legend">
                    <h3 class="text-center text-muted">Graph Legend</h3>
                </div>
            </div>
        </div>
    </div>
    
                        <?php  
                            $errors = new Graphs();
                            $flag = null;
                            if(!empty($_GET)){
                                $flag = $_GET['flag'];
                                
                            }
                            echo $errors->getDefaultGraphData($flag, $_POST);

                        ?>
<script>
   
  var chart = new Morris.Bar({

  element: 'myfirstchart',

  data: <?php echo $errors->getDefaultGraphData($flag, $_POST);?>,

  xkey: 'y',
  ykeys: ['Emergency', 'Urgent','Major','Medium priority', 'Low priority','Minor'],
  labels:  ['Emergency', 'Urgent','Major','Medium', 'Low','Minor'],
  barColors: ['red', 'darkorange','#ffbb33','#b7a108','#f2ce60','#dcc98f'],
  resize: true
});

chart.options.labels.forEach(function(label, i){
    var legendlabel = $('<span style="display: inline-block;">'+label+'</span>')
    var legendItem = $('<div class="mbox"></div>').css('background-color', chart.options.barColors[i]).append(legendlabel);
    $('#legend').append(legendItem);   
})
</script>
<?php

require_once "template/footer.php";

?>