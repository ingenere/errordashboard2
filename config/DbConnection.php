<?php

define("DB_NAME", "errors");
DEFINE("DB_USER", "root");
DEFINE("DB_PASS", "");
DEFINE("DB_HOST", "localhost");

class DbConnection{


private static $connection;


	public static function connectDb(){

		try{

			$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);


			if (!$conn) 
    			die("Connection failed: " . mysqli_connect_error());

		}
		catch(Exception $e){

			die($e->getMessage());
		}

     
		return $conn;
	}

	
}



?>