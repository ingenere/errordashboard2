<?php 
	require_once "config/DbConnection.php";

	class Graphs{

		public static function getDefaultGraphData($flag = null, $search_data = null){


			$conn = DbConnection::connectDb();
      $flags = ['6'=>'Emergency', '5'=>'Urgent', '4'=>'Major','3'=>'Medium priority', '2'=>'Low priority', '1'=>'Minor'];

			$sql = "SELECT * FROM (SELECT  FLAG, count(*) as error_count FROM error where MONTH(HEURE) = MONTH(NOW()) group by FLAG) as graph ORDER BY FLAG";

      if(!is_null($search_data)){
      

        if(!empty($search_data["search_term"]))
        {

            $sql = "SELECT * FROM (SELECT FLAG, count(*) as error_count FROM error where MONTH(HEURE) = MONTH(NOW()) and DESI LIKE '%".$search_data["search_term"]."%' group by FLAG) as graph ORDER BY FLAG";
        } 

        if(!empty($search_data["search_term"]) && !empty($search_data["from_date"]))
        {
          $sql = "SELECT * FROM (SELECT FLAG, count(*) as error_count FROM error WHERE DATE(HEURE) = '".date('Y-m-d', strtotime($search_data["from_date"]))."'"." and DESI LIKE '%".$search_data["search_term"]."%' group by FLAG) as graph ORDER BY FLAG";
            
        }

        if(empty($search_data["search_term"]) && !empty($search_data["from_date"]))
        {
          $sql = "SELECT * FROM (SELECT FLAG, count(*) as error_count FROM error WHERE DATE(HEURE) = '".date('Y-m-d', strtotime($search_data["from_date"]))."'"."  group by FLAG) as graph ORDER BY FLAG";
           
        }

        if(empty($search_data["search_term"]) && !empty($search_data["from_date"]) && !empty($search_data["to_date"]))
        {
          $sql = "SELECT * FROM (SELECT FLAG, count(*) as error_count FROM error WHERE DATE(HEURE)BETWEEN '".date('Y-m-d', strtotime($search_data["from_date"]))."' AND '".date('Y-m-d', strtotime($search_data["to_date"]))."'"."  group by FLAG) as graph ORDER BY FLAG";
          
        }

        if(!empty($search_data["search_term"]) && !empty($search_data["from_date"]) && !empty($search_data["to_date"]))
        {

            $sql = "SELECT * FROM (SELECT FLAG, count(*) as error_count FROM error WHERE DATE(HEURE)BETWEEN '".date('Y-m-d', strtotime($search_data["from_date"]))."' AND '".date('Y-m-d', strtotime($search_data["to_date"]))."'"." and DESI LIKE '%".$search_data["search_term"]."%'  group by FLAG) as graph ORDER BY FLAG";
        }
        
        }

      if ($result=mysqli_query($conn,$sql))
        {
          $count = 0;
          // Fetch one and one row
          $graphData = '[{y:1,';
          while ($row=mysqli_fetch_row($result)){

            $graphData .= "'".$flags[$row[0]]."':". $row[1].",";

          }
          $graphData = rtrim($graphData,',');

         
          $graphData .= '}]';
        }


        return $graphData;

			

    }


}
?>
