<?php 
	require_once "config/DbConnection.php";

	class Errors{

   
		public function getErrors($flag = null, $search_data = null){

      $results_per_page = 20;
    
			$conn = DbConnection::connectDb();

      if (isset($_GET["page"])) 
      {
       $page  = $_GET["page"]; 
      }
       else 
      {
       $page=1; 
      } 
      $start_from = ($page-1) * $results_per_page;

			$sql = "SELECT * FROM error LIMIT $start_from, ".$results_per_page."";

			$errorHtml = "";
      
      $and = "";
      $_SESSION["flag"]=$flag;

			if(!is_null($flag) || isset($_SESSION["flag"])){
				$sql = " SELECT * FROM error WHERE FLAG ='".$flag."'LIMIT $start_from, $results_per_page";
        $and = " AND";
        $_SESSION["flag"]=$flag;
			}


      
      if(count($search_data) != 0){
       
        $sql =  "SELECT * FROM error  WHERE ".$search_data['field']." LIKE '%".$search_data['search_term']."%'LIMIT $start_from, $results_per_page";

        if(!empty($search_data["from_date"]) && empty($search_data["to_date"]))
        {
         
          if  (!is_null($_SESSION["flag"])  && is_null($flag))
          {
           $sql=  "SELECT * FROM error  WHERE ".$search_data['field']." LIKE '%".$search_data['search_term']."%' and FLAG = ".$_SESSION["flag"]." LIMIT $start_from, $results_per_page";
           
          }

          $sql = "SELECT * FROM ($sql) AS first_search WHERE DATE(HEURE) = '".date('Y-m-d', strtotime($search_data["from_date"]))."'LIMIT $start_from, $results_per_page";

            
        }

        if(!empty($search_data["from_date"]) && !empty($search_data["to_date"]))
        {
          if  (!is_null($_SESSION["flag"])  && is_null($flag))
          {
           
           $sql= "SELECT * FROM ($sql) AS first_search WHERE DATE(HEURE) = '".date('Y-m-d', strtotime($search_data["from_date"]))."'AND FLAG =".$_SESSION["flag"]."LIMIT $start_from, $results_per_page";
           

          }

            $sql .= "SELECT * FROM ($sql) WHERE DATE(HEURE) BETWEEN '".date('Y-m-d', strtotime($search_data["from_date"]))."' AND '".date('Y-m-d', strtotime($search_data["to_date"]))."'";
          
        }


        

      }
			
			if ($result=mysqli_query($conn,$sql))
  			{
  				// Fetch one and one row
          $i=0;
  			
  				while ($row=mysqli_fetch_row($result)){
            $i++;
  					$errorHtml .= "<tr>";
  					$errorHtml .= "<td>".$row[1] ."</td>";
  					$errorHtml .= "<td>".$row[2] ."</td>";
  					$errorHtml .= "<td>".$row[3] ."</td>";
  					$errorHtml .= "<td>".$row[4] ."</td>";
            $errorHtml .= "<td>".$row[5] ."</td>";
            $errorHtml .= "<td>".$row[6] ."</td>";
            $errorHtml .= "<td>".$row[7] ."</td>";
            $errorHtml .= "<td>".$row[8] ."</td>";
            $errorHtml .= "<td>".$row[9] ."</td>";
            $errorHtml .= "<td>".$row[10] ."</td>";
            $errorHtml .= "<td>".$row[11] ."</td>";
            $errorHtml .= "<td>".$row[12] ."</td>";
            $errorHtml .= "<td>".$row[13] ."</td>";
  					switch($row[14]){
              case 3: $span = "badge badge-danger";break;
              case 2: $span = "badge badge-warning";break;
  						default: $span = "badge badge-info";break;
  					}
  					$errorHtml .= "<td><span class='$span'>".$row[14] ."</span></td>";
  					$errorHtml .="</tr>";
  				}

              //  $flag=null;
              // if(isset($_GET['flag'])){
              //   $flag = $_GET['flag'];
              // }
              // else if(isset($_SESSION["flag"])){
              //   $flag = $_SESSION["flag"];
              // }
              $tpages =  self::getPages($flag);
          print "<b>"."TOTAL: ".$i." out of ".$tpages;
    
			}


   
		return $errorHtml;
	}



  public static function getPages($flag = null){

   $results_per_page = 20;
   $conn = DbConnection::connectDb();
   $sql = "SELECT COUNT(ID_ERROR) AS total FROM  error";
   
   if(!is_null($flag)){
        $sql = "SELECT COUNT(ID_ERROR) AS total FROM  error WHERE FLAG ="."$flag";
        

      }

      $row[0]=null;

   if ($result=mysqli_query($conn,$sql))
        

      $row = mysqli_fetch_row($result);

  
       
return $row[0];

  }

  public static function getErrorCount($search_data = null){

      $conn = DbConnection::connectDb();

      $sql = "SELECT * FROM (SELECT  FLAG, count(*) as error_count FROM error where MONTH(HEURE) = MONTH(NOW()) group by FLAG) as graph ORDER BY FLAG";

      
      if(isset($search_data) && !empty($search_data)){


          if(!empty($search_data["clientID"]))
        {

           $sql = "SELECT * FROM (SELECT  FLAG, count(*) as error_count FROM error where MONTH(HEURE) = MONTH(NOW()) and CLIENT_ID LIKE '%".$search_data["clientID"]."%'group by FLAG) as graph ORDER BY FLAG";

        } 

          if(!empty($search_data["package"]))
        {

           $sql = "SELECT * FROM (SELECT  FLAG, count(*) as error_count FROM error where MONTH(HEURE) = MONTH(NOW()) and PACKAGE LIKE '%".$search_data["package"]."%'group by FLAG) as graph ORDER BY FLAG";
        } 
          if(!empty($search_data["clientID"]) && !empty($search_data["package"]))
        {

           $sql = "SELECT * FROM (SELECT  FLAG, count(*) as error_count FROM error where MONTH(HEURE) = MONTH(NOW()) and CLIENT_ID LIKE '%".$search_data["clientID"]."%' and PACKAGE LIKE '%".$search_data["package"]."%'group by FLAG) as graph ORDER BY FLAG";

           
        } 

    }

     if ($result=mysqli_query($conn,$sql))
        {
          $errors = [];
          while ($row=mysqli_fetch_row($result)){
            array_push($errors, $row[1]);
          }
         
        }

       

        return $errors;
}

}



?>