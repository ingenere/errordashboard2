<?php

require_once "template/header.php";
require_once "classes/Errors.php";

  //get the count for the various kinds of errors
  $errorCount = Errors::getErrorCount($_POST);
  
?>
<div class="jumbotron">
	<h2 class="text-center">ERRORS DASHBOARD</h2>
</div>

<div class="container">

  <div class="row">
   <div class="col-sm-2 col-xs-12 col-md-2"></div>
  <div class="col-sm-8 col-xs-12 col-md-8">
    <form method="POST" action="dashboard.php" class="filter-form">
        <div class="form-row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Client" name="clientID">
            </div>
            <div class="col">
                <input type="text" class="form-control" placeholder="Package" name="package">
            </div>
            

            <div class="col">
                <button type="submit" class="btn btn-info col-sm-9 col-md-9">Submit</button>
            </div>
        </div>
        </div>
    </form>
<div class="row">
   <div class="col-sm-2 col-xs-12 col-md-2"></div>
  <div class="col-sm-4 col-xs-12 col-md-4">
    <div class="card">
    	<div class="card-header bg-default error-header">
    		All Errors
         <span class="badge error-badge">
          <?php 
            $sum = 0;
          
              foreach ($errorCount as $key => $value) {
                $sum += $value;
              } 
             echo $sum;
            
          ?>
        </span>
  		</div>
      <div class="card-body">
        <p class="card-text">This is a description of the type of errors represented by this card</p>
        <a href="error_list.php" class="btn btn-primary">List Errors</a>
        <a href="error_graphs.php" class="btn btn-primary">View Errors</a>
      </div>
    </div>
  </div>
  <div class="col-sm-4 col-xs-12 col-md-4">
    <div class="card">
    	<div class="card-header bg-danger error-header">
    		 Urgent errors
          <span class="badge error-badge">
          <?php 
          
              echo array_key_exists(2, $errorCount) ? $errorCount[2] : 0; 
          ?>
        </span>
  		</div>
      <div class="card-body">
      
        <p class="card-text">This is a description of the type of errors represented by this card</p>
        <a href="error_list.php?flag=3" class="btn btn-primary">List Errors</a>
        <a href="error_graphs.php" class="btn btn-primary">View Errors</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-sm-2 col-xs-12 col-md-2"></div>
   <div class="col-sm-4 col-xs-12 col-md-4">
    <div class="card">
    	<div class="card-header bg-warning error-header ">
    		Major Errors 
        <span class="badge error-badge">
          <?php 
              echo array_key_exists(1, $errorCount) ? $errorCount[1] : 0; 
          ?>
        </span>
  		</div>
      <div class="card-body">
        <p class="card-text">This is a description of the type of errors represented by this card</p>
        <a href="error_list.php?flag=2" class="btn btn-primary">List Errors</a>
        <a href="error_graphs.php" class="btn btn-primary">View Errors</a>
      </div>
    </div>
  </div>
  
  <div class="col-sm-4 col-xs-12 col-md-4">
    <div class="card">
    	<div class="card-header bg-major error-header">
    		Minor Errors  
         <span class="badge error-badge">
          <?php 
              echo array_key_exists(0, $errorCount) ? $errorCount[0] : 0; 
          ?>
        </span>                  
  		</div>
      <div class="card-body">
        <p class="card-text">This is a description of the type of errors represented by this card</p>
        <a href="error_list.php?flag=1" class="btn btn-primary">List Errors</a>
        <a href="error_graphs.php" class="btn btn-primary">View Errors</a>
      </div>
    </div>
  </div>
</div>
</div>
   
<?php

require_once "template/footer.php";

?>





























